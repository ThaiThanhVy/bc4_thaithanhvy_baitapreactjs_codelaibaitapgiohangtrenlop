import React, { Component } from "react";

let style = {
  border: '1px solid blue'
}

export default class DetailShoe extends Component {
  render() {
    let { image, name, price, description } = this.props.detailShoe;
    return (
      <div className="container p-5">
        <div className="row" style={style}>
          <div className="col-4" style={style} >
            <img src={image} alt="" className="w-100" />
          </div>
          <div className="col-8" style={style} >
            <p>Tên : {name}</p>
            <p>Giá : {price}</p>
            <p>Mô tả : {description}</p>
          </div>
        </div>
      </div>
    );
  }
}
