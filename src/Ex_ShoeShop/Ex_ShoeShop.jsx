import React, { Component } from "react";
import { dataShoe } from "./data_shoe"
import DetailShoe from "./DetailShoe";
import GioHang from "./GioHang";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detailShoe: dataShoe[0],
    gioHang: [],
  };
  handleXemChiTiet = (idShoe) => {
    let index = this.state.shoeArr.findIndex((item) => {
      return item.id == idShoe;
    });

    index !== -1 &&
      this.setState({
        detailShoe: this.state.shoeArr[index],
      });
  };

  handleAddToCart = (shoe) => {
    let cloneGioHang = [...this.state.gioHang];

    let index = this.state.gioHang.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      let spGioHang = { ...shoe, soLuong: 1 };
      console.log("spGioHang: ", spGioHang);
      cloneGioHang.push(spGioHang);
    } else {
      cloneGioHang[index].soLuong++;
    }
    this.setState({
      gioHang: cloneGioHang,
    });
  };

  handleRemove = (idShoe) => {
    let cloneGioHang = [...this.state.gioHang];
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idShoe;
    });
    if (index !== -1) {
      cloneGioHang.splice(index, 1);
    }

    this.setState({
      gioHang: cloneGioHang,
    });
  };

  tangGiamSoLuong = (idSP, tangGiam) => {
    let cloneGioHang = [...this.state.gioHang];
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idSP;
    });
    if (tangGiam) {
      cloneGioHang[index].soLuong += 1
    } else if (cloneGioHang[index].soLuong > 1) {
      cloneGioHang[index].soLuong -= 1
    }

    this.setState({
      gioHang: cloneGioHang,
    });
  };




  render() {

    return (
      <div>
        <GioHang
          handleRemove={this.handleRemove}
          gioHang={this.state.gioHang}
          tangGiamSoLuong={this.tangGiamSoLuong}
        />
        <ListShoe
          data={this.state.shoeArr}
          handleXemChiTiet={this.handleXemChiTiet}
          handleAddToCart={this.handleAddToCart}
        />
        <DetailShoe detailShoe={this.state.detailShoe} />
      </div>
    );
  }
}
